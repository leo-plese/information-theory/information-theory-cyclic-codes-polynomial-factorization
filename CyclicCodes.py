from sympy import *
from sympy import Poly
from itertools import combinations
import numpy as np
import pandas as pd


def prependZerosIfNeeded(polyCoeffs):
    polyCoeffsDiffFromPolDeg = (polDeg + 1) - len(polyCoeffs)
    if polyCoeffsDiffFromPolDeg > 0:
        for i in range(polyCoeffsDiffFromPolDeg):
            polyCoeffs.insert(0, 0)

    return polyCoeffs


def appendZerosIfNeeded(polyCoeffs):
    polyCoeffsDiffFromPolDeg = polDeg - len(polyCoeffs)
    if polyCoeffsDiffFromPolDeg > 0:
        for i in range(polyCoeffsDiffFromPolDeg):
            polyCoeffs.append(0)

    return polyCoeffs

def getFactorProductsSet(factors):
    factorProducts = set()
    factorsCpy = factors[:]
    for i in range(1, len(factorsCpy) + 1):
        combs = combinations(factors, i)

        for comb in combs:
            factorsInComb = list(comb)
            combsProduct = factorsInComb[0]
            for j in range(1, len(factorsInComb)):
                combsProduct = expand(combsProduct * factorsInComb[j])
            factorProducts.add(combsProduct)

    factorProductsSet = set()
    for factor in factorProducts:
        polyn = Poly(factor, x, modulus=2)
        polyCoeffs = polyn.all_coeffs()
        polyCoeffs = prependZerosIfNeeded(polyCoeffs)
        factorProductsSet.add((polyn.args[0], tuple(polyCoeffs)))

    # add factor 1
    oneConstExpr = Add(1)
    onePolyn = Poly(oneConstExpr, x, modulus=2)
    onePolynCoeffs = onePolyn.all_coeffs()
    polyCoeffs = prependZerosIfNeeded(onePolynCoeffs)
    factorProductsSet.add((oneConstExpr, tuple(onePolynCoeffs)))

    return factorProductsSet

def getFactorProductsGMats(m, factorProductsSet):
    factorProductsGMat = []
    for factor in factorProductsSet:
        degr = degree(factor[0], gen=x)
        if degr < m:
            print("FAQ: ", factor, "; ",  [factor[0], list(factor[1])[1:], degr])
            factorProductsGMat.append([factor[0], list(factor[1])[1:], degr])

    factorProductsGMat = sorted(factorProductsGMat, key=lambda fact: fact[2])

    print("factorProductsGMat ====== ", factorProductsGMat)
    return factorProductsGMat

def generateMMatrix(d1, d2):
    messages = np.zeros([d1, d2], dtype=int)
    half = int(d2 / 2)

    for i in range(d1):
        fullN = 0
        newRow = []
        currDig = False
        while fullN < d2:
            for j in range(half):
                newRow.append(1 if currDig else 0)
            fullN += half
            currDig = not currDig
        messages[i] = newRow
        half = int(half / 2)
    messages = messages.T

    return messages

def getCodeWords(messages, genMat):
    msgGenmatDot = np.dot(messages, genMat)
    codeWords = np.remainder(msgGenmatDot, 2)

    return codeWords

def getCodeDistance(codeWords):
    nonZeros = np.count_nonzero(codeWords, axis=1)
    d = np.amin(nonZeros[nonZeros != 0])

    return d

def printTable(nkdTupsForNLst):
    # print("..............................................")
    # for nkdTupsForN in nkdTupsForNLst:
    #     print("n = ", nkdTupsForN[0])
    #     print(nkdTupsForN[1])
    print("..............................................")
    headList = []
    nkdTupsList = []
    for nkdTupsForN in nkdTupsForNLst:
        headList.append("n = " + str(nkdTupsForN[0]))
        tupList = []
        for nkdTup in nkdTupsForN[1]:
            tupList.append(str(nkdTup))
        nkdTupsList.append(tupList)

    nkdLenLst = []
    for lst in nkdTupsList:
        print("-----")
        print(lst)
        nkdLenLst.append(len(lst))

    maxNkdLenLst = max(nkdLenLst)
    for lst in nkdTupsList:
        nkdLenLstDiff = maxNkdLenLst - len(lst)
        addLst = ['' for j in range(nkdLenLstDiff)]
        lst.extend(addLst)

    dictN = dict()
    for i in range(len(headList)):
        dictN[headList[i]] = nkdTupsList[i]

    pd.set_option('display.max_columns', None)
    df = pd.DataFrame.from_dict(dictN)
    print("///////////////////////// [n, k, d] table for 1..n /////////////////////////")
    print(df)

def enterN():
    print("Upisati prirodni broj n do kojeg ce ispisati [n, k, d] tablicu")
    polDegNInput = int(input("n = "))
    while polDegNInput < 1:
        print("Upisati prirodni broj n do kojeg ce ispisati [n, k, d] tablicu")
        polDegNInput = int(input("n = "))

    return polDegNInput


if __name__ == '__main__':
    x = symbols('x')

    polDegNInput = enterN()

    nkdTupsForNLst = []

    for m in range(1, polDegNInput + 1):

        nkdSet = set()
        print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        print("n = ", m)
        polynomial = x ** m - 1

        # get polynomial factorization as a factors list
        factorsLst = Poly(polynomial, modulus=2).factor_list_include()

        ## print((factorsLst))
        polDeg = degree(polynomial, gen=x)
        ## print(polDeg)

        factors = []
        for factor in factorsLst:
            for i in range(factor[1]):
                factors.append(factor[0].args[0])

        # for f in factors:
        #     print("f: " + str(f))

        # get factor products set
        factorProductsSet = getFactorProductsSet(factors)

        for factor in factorProductsSet:
            print("--factor--")
            print(factor)
            print(degree(factor[0], gen=x))

        # get generating matrices G
        factorProductsGMat = getFactorProductsGMats(m, factorProductsSet)

        # print("***********")
        # for factor in factorProductsGMat:
        #     print(factor)
        # print("***********")

        # iterate over G matrices and calculate m dot G matrix product
        for factor in factorProductsGMat:
            # print factor in format [factor polynomial, factor polynomial bit vector representation, factor polynomial degree]
            print("## ", factor)

            r = factor[2]
            k = m - r

            d1 = k
            d2 = 2 ** k

            # generate m (messages) matrix which is to be multiplied by generating matrix
            messages = generateMMatrix(d1, d2)
            ## print("msgs: ", messages)

            genMat = np.zeros([d1, m], dtype=int)

            productRow = factor[1]
            for i in range(d1):
                genMat[i] = productRow[i:] + productRow[:i]

            # flip/reversed row order to be in standard G matrix format
            genMat = np.flip(genMat, axis=0)
            ## print("genMat: ", genMat)

            codeWords = getCodeWords(messages, genMat)
            ## print("DOT: ", codeWords)

            # get code distance
            d = getCodeDistance(codeWords)

            # create new [m, k, d] item for the current code and add it to the set of [m, k, d] for current m
            # note: m is length of cyclic code (usually denoted as n)
            nkdTup = (m, k, d)
            nkdSet.add(nkdTup)
            print("[", m, ",", k, ",", d, "]")

        # for current m sort by k and then by d
        nkdSet = sorted(nkdSet, key=lambda el: (el[1], el[2]))

        # get all [n, k, d] as tuples and store it into a numpy array
        nkdTupsForN = np.array(nkdSet)
        # print("--nkdTupsForN--")
        # print(nkdTupsForN)

        # save the [n, k, d] list for the current n in global [n, k, d] list (for all n)
        nkdTupsForNLst.append((m, nkdTupsForN))

    # print [n, k, d] table
    print("\n")
    printTable(nkdTupsForNLst)

