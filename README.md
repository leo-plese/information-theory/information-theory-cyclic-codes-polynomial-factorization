# Information Theory Cyclic Codes Polynomial Factorization

Generate list of all cyclic codes for each n from 1 to number input by user. Output is in format of [n k d] table (n = codeword length, k = code dimension, d = code distance), columns represent degree of generator polynomial (n).

Implemented in Python using sympy, itertools, numpy and pandas library.

My project in Information Theory, FER, Zagreb.

Created: 2020
